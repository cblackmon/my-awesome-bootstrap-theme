<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package My Awesome Bootstrap Theme
 */
?>

	</div><!-- #content -->
	</div><!-- #page -->
	</div><!-- .container -->
	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="site-info">
			<?php do_action( 'my_awesome_bootstrap_theme_credits' ); ?>
			<a href="http://wordpress.org/" rel="generator"><?php printf( __( 'Powered by %s', 'my-awesome-bootstrap-theme, ' ), 'WordPress,' ); ?></a>
			
			<?php printf( __( 'based on %2$s.', 'my-awesome-bootstrap-theme' ), 'My Awesome Bootstrap Theme', '<a href="http://underscores.me/" rel="designer">Underscores.me</a>' ); ?>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->

<?php wp_footer(); ?>

</body>
</html>
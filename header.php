<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package My Awesome Bootstrap Theme
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div class="container">	
<div id="page" class="hfeed site">
	<?php do_action( 'before' ); ?>
	<header id="masthead" class="site-header" role="banner">
		<div class="site-branding">
			<h1 class="site-title"><span></span><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
		</div>
		<div class="navbar">
		<div class="navbar-inner">	
		<div class="container">
		<nav id="site-navigation" class="main-navigation" role="navigation">
			<h1 class="menu-toggle">
				<button type="button" class="btn btn-default btn-lg">
				  <span class="glyphicon glyphicon-th"></span>
				</button>
			</h1>

			<?php /* Primary navigation */
			wp_nav_menu( array(
			  'theme_location' => 'primary-menu',	
			  'menu' => 'top_menu',
			  'depth' => 2,
			  'container' => false,
			  'menu_class' => 'main-navigation',
			  //Process nav menu using our custom nav walker
			  'walker' => new wp_bootstrap_navwalker())
			);
			?>
		</nav><!-- #site-navigation -->
  	  </div>
	  </div>
      </div> 
	</header><!-- #masthead -->
	

	<div id="content" class="site-content">

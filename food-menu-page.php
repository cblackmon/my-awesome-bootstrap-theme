<?php
/**
 * Template Name: Food Menu Page
 *
 * Selectable from a dropdown menu on the edit page screen.
 *
 * @package My Awesome Bootstrap Theme
 */

get_header(); ?>

<?php
function my_acf_admin_head()
{
	?>
	<style type="text/css">

	}
	
	</style>
 
	<?php
}
 
add_action('acf/input/admin_head', 'my_acf_admin_head');
 
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', 'page' ); ?>
				
				<?php
 			    // Baked Goods Repeater
				if( have_rows('baked_good') ):
				 	// loop through the rows of data
				    while ( have_rows('baked_good') ) : the_row(); ?>
				   	<div class="col-xs-3 field-item">
						<?php the_sub_field('food_name') ?>
					</div>
					<div class="col-xs-3 field-item">
					  <?php the_sub_field('description'); ?>
					</div> 
					<div class="col-xs-3 field-price"> 
					  <?php the_sub_field('price'); ?>
					</div>
					<?php
				 endwhile;
			else : // no rows found
			endif;
				?>
			
				<?php
		    	// Dessert Repeater
				if( have_rows('dessert') ):
			 		// loop through the rows of data
			    	while ( have_rows('dessert') ) : the_row(); ?>
					<div class="col-xs-3 field-item">
						<?php the_sub_field('food_name') ?>
					</div>
			   		<div class="col-xs-3 field-item">
				  	  <?php the_sub_field('description'); ?>
					</div>
					<div class="col-xs-3 field-price">  
				  	  <?php the_sub_field('price'); ?>
					</div>
				<?php
			    endwhile;
			else : // no rows found
			endif;
			?>
		
				<?php
	    		// Juices Repeater
				if( have_rows('juices') ):
		 			// loop through the rows of data
		    	while ( have_rows('juices') ) : the_row(); ?>
			<div class="col-xs-3 field-item">
				<?php the_sub_field('food_name') ?>
			</div>
		   	<div class="col-xs-3 field-item">
			  <?php the_sub_field('description'); ?>
			</div>  
			<div class="col-xs-3 field-price">
			  <?php the_sub_field('price'); ?>
			</div>	
			<?php
		    endwhile;
		else : // no rows found
		endif;
		?>
	<?php endwhile; // end of the loop. ?>
			
			

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>